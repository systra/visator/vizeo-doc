---
home: false
footer: Vizeo-doc

---
# Documentation pour les utilisateurs de Vizeo

Documentation et FAQ pour mieux utiliser Vizeo.


Vizeo est un outil de gestion de Visas. 

Besoin de voir les questions fréquentes ? Direction la [FAQ](/faq/faq.md)!

Au quotidien, des personnes contribuent au projet en [ajoutant des documents](uploader) et [visualisent les documents](visualiser).

D'autres personnes [participent au workflow](workflow-introduction), par exemple en déclarant la [recevabilité d'un document](recevabilite)

Et tout cela aura été praparé lors du paramétrage du projet : on aura [créé le projet](creation-projet) et entré les différents paramètres.

Si vous ne savez pas de quoi on parle, cette image vous le rapellera peut-être (et surtout je veux faire une exemple d'image dans cette page):
![ici un screenshot](./images/screenshot.png)
*L'aspect de l'application lorsqu'on peut accéder à plusieurs projets*


[uploader]: '/contribuer-au-projet/uploader-une-version-de-document.md'
[visualiser]: '/contribuer-au-projet/visualiser-les-documents.md'
[workflow-introduction]: '/gerer-un-workflow/introduction.md'
[recevabilite]: '/gerer-un-workflow/etape-recevabilite.md'
[creation-projet]: '/parametrer-un-projet/creer-un-projet.md'

