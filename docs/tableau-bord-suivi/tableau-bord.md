# Tableau de bord des documents en cours d'instruction


Il s’agit de la première page présentée lorsque vous vous connectez à un projet.

Elle est divisée en 2 parties :
+ Bandeau supérieur, permettant de filtrer la liste de documents selon les métiers contributeurs, les marchés, étapes du circuit de traitement ou encore par mot clé
+ Tableau principal, affichant la liste des documents en cours d’instruction. Par défaut le tableau affiche 50 éléments par page, cette limite est modifiable en bas à droit du tableau

![Tableau de bord de suivi des documents](../images/3-tableau-de-bord.png)
<br><i> Tableau de bord de suivi des documents
