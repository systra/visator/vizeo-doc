# Se connecter à l'outil

L’url de connexion est la suivante : https://vizeo.systra.digital

A la connexion, un message peut apparaitre, il suffit de valider les demandes de connexion pour accéder à l’application :

![Connexion à l'application](../images/1-login-VIZEO.png)
<br><i> Connexion à l'application

Une fois connecté, la liste des projets auxquels vous avez accès apparait, il vous suffit de sélectionner celui auquel vous désirez accéder :

![Liste des projets](../images/2-liste-projets.png)
<br><i> Liste des projets

